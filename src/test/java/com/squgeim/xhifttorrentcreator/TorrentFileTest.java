/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squgeim.xhifttorrentcreator;

import com.squgeim.xhiftbencoder.BencodeDictionary;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author squgeim
 */
public class TorrentFileTest {
    
    static TorrentFile torrent = null;
    
    public TorrentFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            torrent = new TorrentFile("/home/squgeim/pic.jpg", "http://localhost:6969/announce");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TorrentFileTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTorrentGen() throws IOException {
        // TODO review the generated test code and remove the default call to fail.
       BencodeDictionary torrent_dict = torrent.getTorrentDict();
       System.out.println(torrent.getMagnetLink());
        try {
            FileOutputStream out = new FileOutputStream("/home/squgeim/pic.torrent");
            out.write(torrent_dict.getBencodeBytes());
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TorrentFileTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TorrentFileTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
